from flask import Flask, jsonify, request

products = [
    {'id': 1, 'name': 'Product 1'},
    {'id': 2, 'name': 'Product 2'}
]

app = Flask(__name__)



@app.route('/')
def index():
    return "<h1>Hello from our store.see you</h1>"


@app.route('/products')
def get_products():
    return jsonify(products)


@app.route('/product/<int:id>')
def get_product(id):
    for p in products:
        if p['id'] == id:
            return jsonify(p)
            
    return f'Product with id {id} not found', 404
    
#-----------next week-----------------------------------------
@app.route('/product', methods=['POST'])
def post_product():
    # Retrieve the product from the request body
    request_product = request.json

    # Generate an ID for the post
    new_id = len(products)+ 1

    # Create a new product
    new_product = {
        'id': new_id,
        'name': request_product['name']
    }

    # Append the new product to our product list
    products.append(new_product)

    # Return the new product back to the client
    return jsonify(new_product), 201

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0',port=8080)

